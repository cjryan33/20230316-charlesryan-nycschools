package com.charles.virtusa.nycschools.model

import javax.inject.Inject

class Repository @Inject constructor(val apiService: ApiService) {

    suspend fun getNycSchoolsList() = apiService.getNycSchools()

    suspend fun getSatScore(dbn: String) = apiService.getSATScore(dbn)
}