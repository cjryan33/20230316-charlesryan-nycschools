package com.charles.virtusa.nycschools.view.viewholders

import androidx.recyclerview.widget.RecyclerView.ViewHolder
import com.charles.virtusa.nycschools.databinding.ViewHolderSchoolBinding
import com.charles.virtusa.nycschools.model.SchoolInfo

class SchoolDetailsViewHolder(val binding: ViewHolderSchoolBinding) :
    ViewHolder(binding.root) {
    fun bind(school: SchoolInfo) {

        binding.tvSchoolName.text = school.schoolName
        binding.tvLocation.text = "Address: ${school.location}"
        binding.tvPhoneNumber.text = "Phone: ${school.phoneNumber}"

    }
}