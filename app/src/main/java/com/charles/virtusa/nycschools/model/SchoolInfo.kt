package com.charles.virtusa.nycschools.model

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName


data class SchoolInfo(
    @SerializedName("dbn")
    val dbn: String,

    @SerializedName("school_email")
    val schoolEmail: String,

    @SerializedName("school_name")
    val schoolName: String,

    @SerializedName("phone_number")
    val phoneNumber: String,

    @SerializedName("website")
    val website: String,

    @SerializedName("location")
    val location: String


): Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readString() ?: "",
        parcel.readString() ?: "",
        parcel.readString() ?: "",
        parcel.readString() ?: "",
        parcel.readString() ?: "",
        parcel.readString() ?: ""
    ) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(dbn)
        parcel.writeString(schoolEmail)
        parcel.writeString(schoolName)
        parcel.writeString(phoneNumber)
        parcel.writeString(website)
        parcel.writeString(location)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<SchoolInfo> {
        override fun createFromParcel(parcel: Parcel): SchoolInfo {
            return SchoolInfo(parcel)
        }

        override fun newArray(size: Int): Array<SchoolInfo?> {
            return arrayOfNulls(size)
        }
    }
}