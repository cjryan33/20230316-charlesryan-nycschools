package com.charles.virtusa.nycschools.view

import android.content.Intent
import android.os.Bundle
import android.view.View.GONE
import android.view.View.VISIBLE
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.charles.virtusa.nycschools.databinding.ActivityMainBinding
import com.charles.virtusa.nycschools.di.NetworkModule

import com.charles.virtusa.nycschools.view.adapter.SchoolAdapter
import com.charles.virtusa.nycschools.viewmodel.MainViewModel
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class MainActivity : AppCompatActivity() {

    val viewModel: MainViewModel by viewModels()
    lateinit var binding: ActivityMainBinding
    lateinit var adapter: SchoolAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.rvSchools.layoutManager = LinearLayoutManager(baseContext)

        setupObservers()

        viewModel.getNycSchools()
    }


    private fun setupObservers() {
        viewModel.schools.observe(this) {

            adapter = SchoolAdapter(it)
            binding.apply {
                viewGroupProcessing.visibility = GONE
                rvSchools.visibility = VISIBLE

                rvSchools.adapter = adapter

                adapter.setOnSchoolSelectedListener {
                    val sIntent = Intent(baseContext, SchoolDetailsActivity::class.java).apply {
                        putExtra("school", it)
                    }
                    startActivity(sIntent)
                }
            }
        }

        viewModel.processing.observe(this) {
            binding.apply {
                viewGroupProcessing.visibility = VISIBLE
                rvSchools.visibility = GONE

            }
        }

        viewModel.message.observe(this) {
            binding.apply {
                viewGroupProcessing.visibility = GONE
                rvSchools.visibility = GONE
            }
        }
    }
}