package com.charles.virtusa.nycschools.view.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView.Adapter
import com.charles.virtusa.nycschools.databinding.ViewHolderSchoolBinding
import com.charles.virtusa.nycschools.model.SchoolInfo
import com.charles.virtusa.nycschools.view.viewholders.SchoolDetailsViewHolder

class SchoolAdapter(val schools: List<SchoolInfo>):
Adapter<SchoolDetailsViewHolder>(){
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SchoolDetailsViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding = ViewHolderSchoolBinding.inflate(inflater, parent, false)
        return SchoolDetailsViewHolder(binding)
    }

    override fun onBindViewHolder(holder: SchoolDetailsViewHolder, position: Int) {
        holder.bind(schools[position])

        holder.itemView.setOnClickListener {
            if(this::schoolSelected.isInitialized) {
                schoolSelected(schools[position])
            }
        }
    }

    override fun getItemCount() = schools.size

    private lateinit var schoolSelected: (SchoolInfo) -> Unit

    fun setOnSchoolSelectedListener(listener: (SchoolInfo) -> Unit) {
        schoolSelected = listener
    }
}