package com.charles.virtusa.nycschools.viewmodel

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.charles.virtusa.nycschools.model.Repository
import com.charles.virtusa.nycschools.model.SchoolInfo
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class MainViewModel @Inject constructor(val repository: Repository) : ViewModel() {

    private val _schools = MutableLiveData<List<SchoolInfo>>()
    val schools: LiveData<List<SchoolInfo>> = _schools

    private val _message = MutableLiveData<String>()
    val message: LiveData<String> = _message

    private val _processing = MutableLiveData<Boolean>()
    val processing: LiveData<Boolean> = _processing

    private val handler = CoroutineExceptionHandler{ _, exception ->
        _message.postValue("$exception")
    }

    fun getNycSchools() {

        viewModelScope.launch(IO + handler) {
                _processing.postValue(true)

                val response = repository.getNycSchoolsList()
                _processing.postValue(false)

                if (!response.isSuccessful) {
                    _message.postValue("Failed to load data. Please retry.")
                    return@launch
                }

                val schoolList = response.body()

                if (schoolList == null || schoolList.isEmpty()) {
                    _message.postValue("Empty response from server")
                    return@launch
                }

                _schools.postValue(schoolList)
        }
    }
}