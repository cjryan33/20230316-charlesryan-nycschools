package com.charles.virtusa.nycschools.view

import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import com.charles.virtusa.nycschools.R
import com.charles.virtusa.nycschools.databinding.ActivitySchoolDetailsBinding
import com.charles.virtusa.nycschools.model.SchoolInfo
import com.charles.virtusa.nycschools.viewmodel.SchoolInfoViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class SchoolDetailsActivity : AppCompatActivity() {
    lateinit var selectedSchool: SchoolInfo
    val viewModel: SchoolInfoViewModel by viewModels()
    lateinit var binding: ActivitySchoolDetailsBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivitySchoolDetailsBinding.inflate(layoutInflater)
        setContentView(binding.root)

        intent.extras?.getParcelable<SchoolInfo>("school")?.let {
            selectedSchool = it
        }

        setupObservers()
        if (this::selectedSchool.isInitialized) {

            binding.apply {
                tvSchoolName.text = selectedSchool.schoolName
                tvLocation.text = selectedSchool.location
                if (selectedSchool.website.startsWith("http")) {
                    tvWebsite.text = selectedSchool.website
                } else {
                    tvWebsite.text = "https://${selectedSchool.website}"
                }

                tvPhoneNumber.text = selectedSchool.phoneNumber
                tvEmail.text = selectedSchool.schoolEmail
            }
            viewModel.getSATData(selectedSchool.dbn)
        }
    }

    private fun setupObservers() {

        viewModel.processing.observe(this) {
            if (it) {
                binding.apply {
                    viewGroupProcessing.visibility = View.VISIBLE
                    satGroup.visibility = View.GONE

                }
            }
        }
        viewModel.message.observe(this) {
            binding.apply {
                viewGroupProcessing.visibility = View.GONE
                satGroup.visibility = View.GONE

            }

        }
        viewModel.satScore.observe(this) {
            binding.apply {
                viewGroupProcessing.visibility = View.GONE

                if (it.isEmpty()) {
                    Toast.makeText(baseContext, R.string.no_sat_score, Toast.LENGTH_SHORT).show()
                } else {
                    satGroup.visibility = View.VISIBLE
                    val satScore = it[0]
                    tvNumOfTestTakers.text =
                        getString(R.string.number_of_test_takers, satScore.numOftestTakers)
                    tvWritingAvg.text = getString(R.string.writing, satScore.writingAvg)
                    tvMathAvg.text = getString(R.string.math, satScore.mathAvg)
                    tvCriticalReadingAvg.text =
                        getString(R.string.critical_reading, satScore.criticalReadingAvg)
                }

            }

        }
    }
}