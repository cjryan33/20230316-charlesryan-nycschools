package com.charles.virtusa.nycschools.viewmodel

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import com.charles.virtusa.nycschools.TestResponses
import com.charles.virtusa.nycschools.model.Repository
import com.charles.virtusa.nycschools.model.SchoolInfo
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.StandardTestDispatcher
import kotlinx.coroutines.test.TestCoroutineScope
import kotlinx.coroutines.test.runTest
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.junit.MockitoJUnitRunner
import org.mockito.kotlin.doThrow
import org.mockito.kotlin.verify
import retrofit2.Response

@RunWith(MockitoJUnitRunner::class)
class MainViewModelTest {

    @get:Rule
    val rule = InstantTaskExecutorRule()

    @Mock
    lateinit var repository: Repository

    @Mock
    lateinit var schoolsObserver: Observer<List<SchoolInfo>>

    @Mock
    lateinit var errorObserver: Observer<String>

    @OptIn(ExperimentalCoroutinesApi::class)
    @Test
    fun `test getting a valid response from the NYCSchools Api`() {

        runTest(StandardTestDispatcher()) {
            val gson = Gson()

            val typeToken: TypeToken<List<SchoolInfo>> = object : TypeToken<List<SchoolInfo>>() {}

            val response : Response<List<SchoolInfo>> = Response.success(
                gson.fromJson(TestResponses.schoolListResponse, typeToken.type)
            )

            Mockito.`when`(repository.getNycSchoolsList()).thenReturn(response)

            val viewModel = MainViewModel(repository)
            viewModel.schools.observeForever(schoolsObserver)

            viewModel.getNycSchools()

            verify(repository).getNycSchoolsList()

            val expected: List<SchoolInfo> = gson.fromJson(TestResponses.schoolListResponse, typeToken.type)

            verify(schoolsObserver).onChanged(expected)

            viewModel.schools.removeObserver(schoolsObserver)
        }
    }

    @OptIn(ExperimentalCoroutinesApi::class)
    @Test
    fun `test exception handling for NYCSchools Api`() {
        runTest(StandardTestDispatcher()) {
            doThrow(RuntimeException("No Internet Connection Found")).`when`(repository).getNycSchoolsList()

            val viewModel = MainViewModel(repository)
            viewModel.message.observeForever(errorObserver)

            viewModel.getNycSchools()

            verify(repository).getNycSchoolsList()

            val expectedException = RuntimeException("No Internet Connection Found")

            verify(errorObserver).onChanged("$expectedException")

            viewModel.message.removeObserver(errorObserver)
        }
    }
}